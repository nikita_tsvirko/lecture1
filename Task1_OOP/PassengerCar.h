#pragma once
#include <iostream>
#include "ICar.h"

using namespace std;

class PassengerCar : public ICar {
protected:
	int capacity;
public:
	PassengerCar();
	int GetCapacity();
	void Scan();
	void Print();
	int RentalPriceOfDay();
};
