#pragma once
#include <iostream>
#include "ICar.h"

using namespace std;

class FreightCar : public ICar {
protected:
	int tonnage;
public:
	FreightCar();
	int GetTonnage();
	void Scan();
	void Print();
	int RentalPriceOfDay();
};