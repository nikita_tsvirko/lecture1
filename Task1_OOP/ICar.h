#pragma once
#include <iostream>

using namespace std;

class ICar {
protected:
	int numOfCar;
	double acceleration,
		outgo;
	bool use;
	int daysToUse;
public:
	~ICar() {}
	int GetNumOfCar();
	double GetAcceleration();
	double GetOutgo();
	bool GetUse();
	int GetDaysToUse();
	void SetUse(bool newUse);
	void SetDaysToUse(int newDaysToUse);
	void UpdateDataOfCar();
	virtual void Scan() = 0;
	virtual void Print() = 0;
	virtual int RentalPriceOfDay() = 0;
};