#pragma once
#include <iostream>
#include <string>

using namespace std;

class Client {
	int number;
	string name;
	int numOfCar;
public:
	Client();
	int GetNumber();
	string GetName();
	int GetNumOfCar();
	void Scan();
	void Print();
};