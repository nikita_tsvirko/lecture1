#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include "ICar.h"

using namespace std;

int ICar::GetNumOfCar()
{
	return numOfCar;
}

double ICar::GetAcceleration()
{
	return acceleration;
}

double ICar::GetOutgo()
{
	return outgo;
}

bool ICar::GetUse()
{
	return use;
}

int ICar::GetDaysToUse()
{
	return daysToUse;
}

void ICar::SetUse(bool newUse)
{
	use = newUse;
}

void ICar::SetDaysToUse(int newDaysToUse)
{
	daysToUse = newDaysToUse;
}

void ICar::UpdateDataOfCar()
{
	daysToUse--;
	if (daysToUse == 0) {
		use = false;
	}
}