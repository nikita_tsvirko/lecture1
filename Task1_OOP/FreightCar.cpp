#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include "FreightCar.h"

using namespace std; 

FreightCar::FreightCar()
{
	use = false;
}

int FreightCar::GetTonnage()
{
	return tonnage;
}

void FreightCar::Scan()
{
	cout << "������� ����� ����������, ������, ���������������� � ������\n";
	cin >> numOfCar >> acceleration >> tonnage >> outgo;
}

void FreightCar::Print()
{
	cout << "�������� ���������� � �������: " << numOfCar << endl;
	cout << "������ ����������: " << acceleration << endl;
	cout << "���������������� ����������: " << tonnage << endl;
	cout << "������ ����������: " << outgo << endl;
	if (use) {
		cout << "���������� ��������� � ������, ���� ��������: " << daysToUse << endl;
	}
	else
	{
		cout << "���������� �� ��������� � ������\n";
	}
	cout << endl;
}

int FreightCar::RentalPriceOfDay()
{
	return outgo * tonnage + 200;
}