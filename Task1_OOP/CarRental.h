#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "ICar.h"
#include "PassengerCar.h"
#include "FreightCar.h"
#include "Client.h"

using namespace std;

class CarRental {
	string name;
	int numOfCars;
	vector<Client> clients;
	ICar * carPark[100];
public:
	CarRental();
	void AddCar();
	void AddClient();
	bool TakeCar(int numOfCar);
	void UpdateData();
	void Scan();
	void Print();
};