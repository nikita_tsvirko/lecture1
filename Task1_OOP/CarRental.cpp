#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <iterator>
#include "CarRental.h"

using namespace std;

CarRental::CarRental() {
}

void CarRental::AddCar()
{
	cout << "������� ��� ����������(1-��������, 2-��������): \n";
	int typeCar;
	cin >> typeCar;
	if (typeCar == 1) {
		carPark[numOfCars] = new PassengerCar;
	}
	else {
		carPark[numOfCars] = new FreightCar;
	}
	carPark[numOfCars]->Scan();
	numOfCars++;
}

void CarRental::AddClient()
{
	Client newClient;
	newClient.Scan();
	if (TakeCar(newClient.GetNumOfCar())) {
		clients.push_back(newClient);
	}
}

bool CarRental::TakeCar(int numOfCar)
{
	int i = 0;
	while (i<numOfCars && carPark[i]->GetNumOfCar() != numOfCar) {
		i++;
	}
	if (i == numOfCars || numOfCars == 0 || carPark[i]->GetNumOfCar() != numOfCar){
		cout << "������! ���������� � ����� ������� �� ������\n";
		return false;
	}
	else
	{
		if (carPark[i]->GetUse()) {
			cout << "������! ���������� � ����� ������� ��� � ������\n";
			return false;
		}
		cout << "������� ���������� ���� ������\n";
		int rentalDays;
		cin >> rentalDays;
		carPark[i]->SetUse(true);
		carPark[i]->SetDaysToUse(rentalDays);
		cout << "������ ������� ��������, ��������� ������ " << carPark[i]->RentalPriceOfDay() * rentalDays << endl;
		return true;
	}
}

void CarRental::UpdateData()
{
	for (int i = 0; i < numOfCars; i++) 
		if(carPark[i]->GetUse())
		{
			carPark[i]->UpdateDataOfCar();
			if (!carPark[i]->GetUse()) {
				vector<Client>::iterator j;
				for (j = clients.begin(); j < clients.end(); j++) {
					if (j->GetNumOfCar() == carPark[i]->GetNumOfCar()) {
						clients.erase(j);
						break;
					}
				}
			}
		}
	cout << "���������� � ������ ������ ���������\n";
}

void CarRental::Scan()
{
	cout << "������� �������� ����� �� ������� �����������\n";
	getline(cin, name, '\n');
	cout << "������� ���������� �����������\n";
	cin >> numOfCars;
	for (int i = 0; i < numOfCars; i++) {
		cout << "������� ���������� � " << i + 1 << " ����������\n";
				int typeCar;
		while (true) {
			cout << "������� ��� ����������(1-��������, 2-��������): \n";
			cin >> typeCar;
			if (typeCar == 1) {
				carPark[i] = new PassengerCar;
				break;
			}
			else
			if (typeCar == 2) {
				carPark[i] = new PassengerCar;
				break;
			}
			else {
				cout << "������ ������������ ��� ����������" << endl;
			}
		}
		carPark[i]->Scan();
	}
	cout << endl;
}

void CarRental::Print()
{
	if (name.empty()) {
		cout << "���������� � ����� ��� �� ���� �������\n";
		return;
	}
	cout << "�������� ����� �� ������� �����������: " << name << endl;
	cout << "���������� �����������: " << numOfCars << endl << endl;
	for (int i = 0; i < numOfCars; i++) {
		cout << "���������� � " << i + 1 << " ����������\n";
		carPark[i]->Print();
	}
	cout << endl;
	cout << "���������� ��������: " << clients.size() << endl << endl;
	for (int i = 0; i < clients.size(); i++) {
		cout << "���������� � " << i + 1 << " �������\n";
		clients[i].Print();
	}
}


