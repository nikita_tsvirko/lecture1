#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include <stdio.h>
#include "PassengerCar.h"

using namespace std;

PassengerCar::PassengerCar()
{
	use = false;
}

int PassengerCar::GetCapacity()
{
	return capacity;
}

void PassengerCar::Scan()
{
	cout << "������� ����� ����������, ������, ����������� � ������" << endl;
	cin >> numOfCar >> acceleration >> capacity >> outgo;
}

void PassengerCar::Print()
{
	cout << "�������� ���������� � �������: " << numOfCar << endl;
	cout << "������ ����������: " << acceleration << endl;
	cout << "����������� ����������: " << capacity << endl;
	cout << "������ ����������: " << outgo << endl;
	if (use) {
		cout << "���������� ��������� � ������, ���� ��������: " << daysToUse << endl;
	}
	else
	{
		cout << "���������� �� ��������� � ������\n";
	}
	cout << endl;
}

int PassengerCar::RentalPriceOfDay()
{
	return acceleration * capacity * 2 + 100;
}
