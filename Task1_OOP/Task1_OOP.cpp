#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include "CarRental.h"

using namespace std;

int main()
{
	SetConsoleOutputCP(1251);
	CarRental CarRentalFirm;
	CarRentalFirm.Scan();
	bool work = true;

	while (work) {
		cout << "1. Вывести информацию о фирме проката\n"
			<< "2. Добавить новый автомобиль\n"
			<< "3. Добавить клиента\n"
			<< "4. Следующий день\n"
			<< "5. Выход\n"
			<< "Введите номер команды\n";
		int command;
		cin >> command;
		switch (command)
		{
		case 1:
		{
			CarRentalFirm.Print();
			break;
		}
		case 2:
		{
			CarRentalFirm.AddCar();
			break;
		}
		case 3:
		{
			CarRentalFirm.AddClient();
			break;
		}
		case 4:
		{
			CarRentalFirm.UpdateData();
			break;
		}
		case 5:
		{
			work = false;
			break;
		}
		default:
			cout << "Введена некорректная команда\n";
			break;
		}
		cout << endl;
	}

	return 0;
}